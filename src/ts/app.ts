///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

/**
 * Created by ralopezn on 25/03/2015.
 */
((): void => {

    angular.module('app', ['ionic', 'ngCordova', 'app.dependencies', 'uiGmapgoogle-maps'])

        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                  //  StatusBar.styleDefault();
                }
            });
        })
        .config((uiGmapGoogleMapApiProvider) => {
            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyBnbLbGlEkm0F8IYGnyv3DCAarqvhyGPXQ',
                v: '3.17',
                libraries: 'geometry,visualization'
            });
        })
        .config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) => {
            $urlRouterProvider.otherwise('/map');
            $stateProvider
                .state('map', {
                    url: '/map',
                    templateUrl: 'templates/map-template.html',
                    controller: 'app.controllers.MapController as mapController'
                });
        }]);
})();